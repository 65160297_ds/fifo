/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.fifo;

/**
 *
 * @author Name
 */
class Link {
    public long dData; // data item
    public Link next; // next link in list
    // -------------------------------------------------------------
    public Link(long d) // constructor
    {
        dData = d;
    }
    // -------------------------------------------------------------
    public void displayLink() // display this link
    {
        System.out.print(dData + " ");
    }
}